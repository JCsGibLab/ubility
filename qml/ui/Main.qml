/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Qt.labs.platform 1.0
import Ubility 1.0 as Ubility
import "pages"
import "components"

UITK.MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'ubility.hummlbach'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    UITK.AdaptivePageLayout {
        id: pageLayout
        anchors.fill: parent
        primaryPage: menuPage

        layouts: UITK.PageColumnsLayout {
            when: width > units.gu(80)
            UITK.PageColumn {
                minimumWidth: units.gu(30)
                maximumWidth: units.gu(60)
                preferredWidth: units.gu(40)
            }
            UITK.PageColumn {
                fillWidth: true
            }
        }

        MenuPage {
            id: menuPage
            clients: store.clients
            bills: store.bills

            onRemoveClient: {
                store.removeClient(clientIndex, clientId);
            }

            onAddClient: {
                addClientPage.reload(null);
                pageLayout.addPageToNextColumn(pageLayout.primaryPage, addClientPage.item);
            }

            onEditClient: {
                store.setCurrentClient(clientIndex);
                addClientPage.reload(store.currentClient);
                pageLayout.addPageToNextColumn(pageLayout.primaryPage, addClientPage.item);
            }

            onShowBillItemsByClient: {
                store.setCurrentClient(clientIndex);
                billItemsListPage.clientName = store.currentClient.firstname + " " + store.currentClient.lastname;
                pageLayout.addPageToNextColumn(pageLayout.primaryPage, billItemsListPage);
            }

            onShowBillItemsByBill: {
                store.setCurrentBill(billIndex);
                billItemsListPage.billId = store.currentBill.id;
                pageLayout.addPageToNextColumn(pageLayout.primaryPage, billItemsListPage);
            }

            onCancelBill: {
                store.cancelBill(billIndex, billId);
            }

            onUndoBill: {
                store.undoBill(billIndex, billId);
            }

            onGoSettings: {
                pageLayout.addPageToNextColumn(pageLayout.primaryPage, settingsPage);
            }
        }

        Loader {
            id: addClientPage
            
            function reload(editedClient) {
                source = "";
                source = "pages/AddClientPage.qml";
                item.editedClient = editedClient;
            }

            Connections {
                target: addClientPage.item

                onAddClient: {
                    store.addClient(client);
                }

                onUpdateClient: {
                    store.updateCurrentClient(client);
                }
            }
        }

        BillItemsListPage {
            id: billItemsListPage
            billItems: store.billItems
            products: store.products

            //onAddBillItem: {
            //    pageLayout.addPageToNextColumn(pageLayout.primaryPage, productsListPage);
            //}

            onPrintBill: {
                var file = store.addBill(biller);
                fileShareComponent.share(file);
            }

            onReprintBill: {
                store.setBill(index);
                var file = store.currentBillToPdf(biller);
                fileShareComponent.share(file);
            }
        }
        
        Settings {
            id: billerSettings
            category: "Biller"
            property alias company: biller.company
            property alias title: biller.title
            property alias firstname: biller.firstname
            property alias lastname: biller.lastname
            property alias street: biller.street
            property alias houseNumber: biller.houseNumber
            property alias postalCode: biller.postalCode
            property alias city: biller.city
            property alias phoneNumber: biller.phoneNumber
            property alias faxNumber: biller.faxNumber
            property alias iban: biller.iban
            property alias bic: biller.bic
            property alias bank: biller.bank
            property alias taxId: biller.taxId
            property alias currency: biller.currency
        }

        Settings {
            id: windowSettings
            category: "Window"
            property alias x: root.x
            property alias y: root.y
            property alias width: root.width
            property alias height: root.height
        }

        SettingsPage {
            id: settingsPage

            biller: biller
            products: store.products

            onOpenAbout: {
                pageLayout.addPageToNextColumn(settingsPage, aboutPage);
            }

            // TODO: move to Ubility.Store (ran into binding salad)
            Ubility.Biller {
                id: biller
            }
        }

        AboutPage {
            id: aboutPage
        }

        ContentPeerPickerPage {
            id: contentPeerPickerPage

            onClosed: {
                pageLayout.removePages(contentPeerPickerPage);
            }
        }
    }

    Ubility.Store {
        id: store
    }

    FileShareComponent {
        id: fileShareComponent

        onOpenUrlExternally: {
            Qt.openUrlExternally("file://"+filename);
        }

        onOpenContentPeerPickerPage: {
            contentPeerPickerPage.sharedUrl = "file://"+filename;
            pageLayout.addPageToNextColumn(pageLayout.primaryPage, contentPeerPickerPage);
        }
    }
}
