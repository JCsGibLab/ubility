import QtQuick 2.7
import Ubuntu.Content 1.3
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2


UITK.Page {
    id: picker
    property var activeTransfer
    property string sharedUrl: ""

    signal closed();

    header: UITK.PageHeader {
        id: header
        title: i18n.tr('Share bill to...')

        leadingActionBar.actions: [
            UITK.Action {
                id: cancel
                objectName: "cancel"
                text: i18n.tr("Cancel")
                iconName: "close"
                onTriggered: {
                    closed();
                }
            }
        ]
    }

    Rectangle {
        id: pickerRectangle
        anchors.fill: parent
        anchors.topMargin: header.height

        ContentPeerPicker {
            id: peerPicker
            anchors.fill: parent
            showTitle: false
            contentType: ContentType.Documents
            handler: ContentHandler.Destination

             onPeerSelected: {
                peerPicker.requestTransfer();
            }
   
            function requestTransfer() {
                picker.activeTransfer = peer.request();
                picker.activeTransfer.stateChanged.connect(function() {
                   if (picker.activeTransfer.state === ContentTransfer.InProgress) {
                        picker.activeTransfer.items = [
                            resultComponent.createObject(parent, {"url": sharedUrl})
                        ];
                        picker.activeTransfer.state = ContentTransfer.Charged;
                        closed();
                    }
                });
            }

            onCancelPressed: {
                closed();
            }
        }
    }

    ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: picker.activeTransfer
    }

    Component {
        id: resultComponent
        ContentItem {}
    }
}
