/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>

#include "ubilitydatabase.h"
#include "clientsmodel.h"
#include "billitemsmodel.h"
#include "billsmodel.h"
#include "biller.h"
#include "productsmodel.h"


int main(int argc, char *argv[])
{
    QApplication *app = new QApplication(argc, (char**)argv);
    app->setApplicationName("ubility.hummlbach");

    qmlRegisterType<UbilityDatabase>("Ubility", 1, 0, "Store");
    qmlRegisterType<Biller>("Ubility", 1, 0, "Biller");
    qmlRegisterType<Client>("Ubility", 1, 0, "Client");
    qRegisterMetaType<ClientsModel>("ClientsModel");
    qRegisterMetaType<BillItemsModel>("BillItemsModel");
    qRegisterMetaType<BillsModel>("BillModell");
    qRegisterMetaType<BillItem>();
    qRegisterMetaType<BillItem::Status>("BillItemStatus");
    qRegisterMetaType<Bill>();
    qRegisterMetaType<ProductsModel>("ProductsModel");
    qRegisterMetaType<Product>();

    QQuickView *view = new QQuickView();
    view->setSource(QUrl(QStringLiteral("ui/Main.qml")));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return app->exec();
}
