#ifndef CLIENTSMODEL_H
#define CLIENTSMODEL_H

#include <QSqlTableModel>
#include <QSqlRecord>

class Client: public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id MEMBER id)
    Q_PROPERTY(QString firstname MEMBER firstname)
    Q_PROPERTY(QString lastname MEMBER lastname)
    Q_PROPERTY(QString street MEMBER street)
    Q_PROPERTY(QString houseNumber MEMBER houseNumber)
    Q_PROPERTY(QString postalCode MEMBER postalCode)
    Q_PROPERTY(QString city MEMBER city)
    Q_PROPERTY(QString phoneNumber MEMBER phoneNumber)

public:
    explicit Client(QObject *parent = 0) : QObject(parent) {}

    QString firstname;
    QString lastname;
    QString street;
    QString houseNumber;
    QString postalCode;
    QString city;
    QString phoneNumber;
    int id;
};


class ClientsModel : public QSqlTableModel
{
    Q_OBJECT
    Q_PROPERTY(int currentClientIndex READ currentClientIndex WRITE setCurrentClientIndex NOTIFY currentClientIndexChanged)
    Q_PROPERTY(QString searchFilter READ searchFilter WRITE setSearchFilter NOTIFY searchFilterChanged)

public:
    explicit ClientsModel(QSqlDatabase database, QObject *parent = 0);
    explicit ClientsModel(const ClientsModel & clientsModel) {}
    explicit ClientsModel() {}

    virtual ~ClientsModel();

    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    Q_INVOKABLE bool add(const Client &client);
    Q_INVOKABLE bool update(int row, const Client &client);
    Q_INVOKABLE bool remove(int row);

    int currentClientIndex() const
    {
        return m_currentClientIndex;
    }

    void setCurrentClientIndex(int index)
    {
        m_currentClientIndex = index;
    }

    QString searchFilter() const
    {
        return m_searchFilter;
    }

    void setSearchFilter(const QString & filter);

Q_SIGNALS:
    void databaseChanged();
    void currentClientIndexChanged();
    void searchFilterChanged();

private:
    QSqlRecord createRecord(const Client &client) const;
    int m_currentClientIndex;
    QString m_searchFilter;
};

#endif
