#include "clientsmodel.h"

#include <QDebug>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlField>

ClientsModel::ClientsModel(QSqlDatabase database, QObject *parent)
    : QSqlTableModel(parent, database)
{
    QSqlQuery query(database);
    if (!query.exec("create table if not exists clients "
               "(cid integer primary key, "
               "firstname varchar(30), "
               "lastname varchar(30), "
               "street varchar(50), "
               "houseNumber varchar(10), "
               "postalCode varchar(10), "
               "city varchar(30), "
               "phoneNumber varchar(20))")) {
        qWarning() << "clients table creation: " << lastError().text();
        return;
    }

    setTable("clients");
    if (!select()) {
        qWarning() << "Failed to select clients table: " << lastError().text();
    }
    sort(2, Qt::AscendingOrder);
}

QVariant ClientsModel::data(const QModelIndex &index, int role) const
{
    QVariant value;

    if (index.isValid()) {
        if (role < Qt::UserRole) {
            value = QSqlTableModel::data(index, role);
        } else {
            int columnIdx = role - Qt::UserRole - 1;
            QModelIndex modelIndex = this->index(index.row(), columnIdx);
            value = QSqlTableModel::data(modelIndex, Qt::DisplayRole);
        }
    }
    return value;
}

QHash<int, QByteArray> ClientsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    for (int i = 0; i < this->record().count(); i++) {
        roles.insert(Qt::UserRole + i + 1, record().fieldName(i).toUtf8());
    }
    return roles;
}

QSqlRecord ClientsModel::createRecord(const Client &client) const
{
    QSqlRecord newRecord;
    newRecord.append(QSqlField("firstname", QVariant::String));
    newRecord.append(QSqlField("lastname", QVariant::String));
    newRecord.append(QSqlField("street", QVariant::String));
    newRecord.append(QSqlField("houseNumber", QVariant::String));
    newRecord.append(QSqlField("postalCode", QVariant::String));
    newRecord.append(QSqlField("city", QVariant::String));
    newRecord.append(QSqlField("phoneNumber", QVariant::String));
    newRecord.setValue("firstname", client.firstname);
    newRecord.setValue("lastname", client.lastname);
    newRecord.setValue("street", client.street);
    newRecord.setValue("houseNumber", client.houseNumber);
    newRecord.setValue("postalCode", client.postalCode);
    newRecord.setValue("city", client.city);
    newRecord.setValue("phoneNumber", client.phoneNumber);
    return newRecord;
}

bool ClientsModel::add(const Client &client)
{
    QSqlRecord newRecord = createRecord(client);
    if (!insertRecord(-1, newRecord)) {
        qWarning() << "Failed to insert record into client database: " << lastError().text();
        return false;
    }
    select();
    return true;
}

bool ClientsModel::update(int row, const Client &client)
{
    QSqlRecord updatedRecord = createRecord(client);
    if (!setRecord(row, updatedRecord)) {
        qWarning() << "Failed to update record in row " << row << ". Error: " << lastError().text();
        return false;
    }
    select();
    return true;
}

bool ClientsModel::remove(int row)
{
    beginRemoveRows(QModelIndex(), row, row);
    removeRow(row);
    endRemoveRows();
    return true;
}

void ClientsModel::setSearchFilter(const QString & filter)
{
    if (filter.simplified().isEmpty()) {
        setFilter("firstname like '%%'");
        return;
    }
    QString filterCondition = QString("firstname like '%1%%' or lastname like '%2%%'").arg(filter).arg(filter);
    setFilter(filterCondition);
}

ClientsModel::~ClientsModel() {
    database().close();
}
